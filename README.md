# About

This is my submission to the following Sqreen home test assignment. It contains
the source code of a Go package called `sqreen` allowing to insert a custom HTTP
header in a `http.Server`. The repository includes everything required to test
it in a dockerized environment.

This README file explains how the submission works, what is inside the
repository, and finally answers the assignment questions.

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [About](#about)
- [Assignment: Go software engineer job offer exercise](#assignment-go-software-engineer-job-offer-exercise)
    - [Contact](#contact)
    - [Duration](#duration)
    - [Exercise](#exercise)
    - [Result](#result)
    - [This is an exercise](#this-is-an-exercise)
- [Content](#content)
- [Development](#development)
    - [Requirements](#requirements)
    - [Continuous integration pipeline](#continuous-integration-pipeline)
    - [Unit and functional tests](#unit-and-functional-tests)
    - [Validation test](#validation-test)
- [Design Decisions](#design-decisions)
    - [Development environment](#development-environment)
    - [Implementation](#implementation)
- [Questions](#questions)
    - [How could the steps taken by the user be made simpler?](#how-could-the-steps-taken-by-the-user-be-made-simpler)
    - [Has the Go language any specifics that prevent making this fully transparent to the user?](#has-the-go-language-any-specifics-that-prevent-making-this-fully-transparent-to-the-user)

<!-- markdown-toc end -->


# Assignment: Go software engineer job offer exercise

This exercise is sent as part of our hiring process at
[Sqreen](https://www.sqreen.io)

## Contact

[jobs@sqreen.io](mailto:jobs@sqreen.io)

## Duration

About 2 hours

## Exercise

The goal of the exercise is to develop a Go package that is able to insert a
custom HTTP header in a Go server.

When running an HTTP server, the following occurs:

    $ curl -I http://<my-host>:8888/
    HTTP/1.1 200 OK
    Date: Wed, 13 Apr 2016 22:08:19 GMT
    Connection: keep-alive

We would like to make it return one additional header:

    $ curl -I http://<my-host>:8888/
    HTTP/1.1 200 OK
    [...]
    X-Instrumented-By: Sqreen

The package has to hook the Go HTTP API, in the simplest form as possible for
the user.

Then please answer the following questions:

 - How could the steps taken by the user be made simpler?
 - Has the Go language any specifics that prevent making this fully
   transparent to the user?

## Result

Please provide us an Go package that will perform the instrumentation, once
required in the HTTP server code.

## This is an exercise

The code you will produce is part of our technical interview process. You
obviously own full property on everything you produce.

# Content

```
.
├── Dockerfile
├── Makefile
├── README.md
├── .gitlab-ci.yml
├── src
│   ├── example
│   └── sqreen
└── test
```

The repository is pretty straightforward and includes only strictly necessary
files:

- Makefile: set of commands using Docker and the Dockerfile.
- Dockerfile: dockerized development and testing environments, using docker's
  multi-stage builds.
- .gitlab-ci.yml: simple CI pipeline for GitLab.
- src/: source files.
  - example/: usage example, also used to validate the complete solution.
  - sqreen/: package's source code.

# Development

## Requirements

Everything is "containerized" and simply executed from a Makefile. Which
restricts the required tools to:

- GNU Make
- Docker

## Continuous integration pipeline

File `.gitlab-ci.yml` defines a simple GitLab CI pipeline with just a single
stage running the tests. To reuse it, simply push it to your gitlab.com
repository and it should automatically detect the file and use it. Note that it
uses a custom docker image I modified to include GNU Make in it.

## Unit and functional tests

Run:

```console
$ make test
docker build --rm --no-cache --target=test -t sqreen-test .
...
```

It uses the Dockerfile and builds its test stage, which includes every required
Go testing tools. It runs the Go test files `*_test.go` included in the source
tree.

## Validation test

To perform an overall validation test, based on a binary program, the following
command runs a docker build container including curl, grep and bash to send a
request to the server and check that the result contains the header.

Run:

```console
$ make test-validation
docker build  --rm --no-cache --target=test-validation -t sqreen-test-validation .
...
```

# Design Decisions

## Development environment

The development environment was designed to be simple, i.e. only with the
necessary tools and using them the simplest way:

- Here, tests are run in a docker build container. For bigger projects, I rather
  mount the source tree as a container volume. It allows faster compilation time
  (files are no longer copied) and outputs everything in-place.  

- Here, the Makefile directly calls docker. For bigger projects, I rather
  recursively call the Makefile (e.g. make target => docker exec … make target
  => target recipe) to make the Makefile the single source of truth and to make
  benefit from its file-dependency management and parallel compilation features. I
  avoided it here as it requires [extra Makefile
  hacks](https://github.com/farjump/Makefile.in/blob/dev/src/mk/toolchain/docker.mk).

- I used the bare Go test framework which mainly lacks from assertion libraries.
  For bigger projects, I rather use a complete Go test framework such as
  [Ginkgo](https://github.com/onsi/ginkgo).

## Implementation

The source code implements the HTTP hook using the decorator design pattern, the
functional programming way: wrapping user's HTTP handler in an anonymous
function. This is Go's current way of doing it, as showed [by the
documentation](https://godoc.org/net/http#Handler) and the handler function
decorators it provides (`StripPrefix`, etc.).

# Questions

## How could the steps taken by the user be made simpler?

This implementation makes developers explicitly wrap their handler functions.
The convenience function `agent.HTTPHandlerFunc()` allows developers to just
replace their `http.HandlerFunc(...)` by `agent.HTTPHandlerFunc()` to obtain the
same handler but now going through this package's features.

Another extreme way would be to provide the same API as Go's `http` package so
that developers could just replace `import "net/http"` by, for example, `import
"sqreen/http"` and make benefit from this instrumented version, of course based
on the official package `http`.

## Has the Go language any specifics that prevent making this fully transparent to the user?

The hooks definitely need to be called. But current Go's implementation of
package `http` doesn't provide globally programmable hook points, as they made
the decision that middleware providers should rather decorate existing HTTP
handlers, which is explicit.

With global hook points, we could instead automatically hook to the `http` package
from package initialization function `func init() { http.Hook.Add(...) }`.

Otherwise, the last implementation proposal of previous question (provide the
same `http` API), would be the most transparent for the user, as it minimizes
required modifications to a single line of code.
