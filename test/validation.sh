#! /usr/bin/env bash
set -e

echo '=== RUN Validation Test'

/go/bin/example &
response=$(curl -is http://localhost:8888/)
echo "$response"
echo $response | grep -qP '(?s)HTTP/1.1 200 OK.+X-Instrumented-By: Sqreen.+My body'
# `set -e` checks the return value and aborts on error
echo === PASS
