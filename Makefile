project/name := sqreen

.PHONY: all
all: docker-build

.PHONY: test
test:
	docker build --rm --no-cache --target=$@ -t $(project/name)-$@ .

.PHONY: test-validation
test-validation:
	docker build --rm --no-cache --target=$@ -t $(project/name)-$@ .
