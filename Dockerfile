FROM golang:1.10 AS dev-base
COPY src/. /go/src/
WORKDIR /go/src/sqreen

FROM dev-base AS test
RUN go test -v -timeout 30s ./...

FROM dev-base AS test-validation
RUN go install -v ./../example
ADD test/validation.sh /tmp/
RUN /tmp/validation.sh
