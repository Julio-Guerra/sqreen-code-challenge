package main

import (
	"log"
	"net/http"
	"sqreen/agent"
)

func main() {
	http.Handle("/", agent.HTTPHandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		w.Write([]byte("My body\n"))
	}))
	log.Fatal(http.ListenAndServe(":8888", nil))
}
