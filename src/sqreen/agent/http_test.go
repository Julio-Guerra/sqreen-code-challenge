package agent

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"sqreen/test"
	"testing"
)

func TestHTTPHandler(t *testing.T) {
	t.Run("unit test", func(t *testing.T) {
		// Test *http.Request
		req := httptest.NewRequest("GET", "https://example.com/example", nil)
		// Test http.ResponseWriter
		w := httptest.NewRecorder()
		// Test user handler
		userHandler := http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
			w.WriteHeader(http.StatusOK)
		})

		// Wrap userHandler
		sqreen := HTTPHandler(userHandler)
		// Test the handler.
		sqreen.ServeHTTP(w, req)

		// Check the result: header X-Instrumented-By exists and is set to `Sqreen`.
		res := w.Result()
		expected := "Sqreen"
		if got := res.Header.Get("X-Instrumented-By"); got != expected {
			test.Fail(t, expected, got)
		}
		fmt.Println()
	})

	t.Run("functional test", func(t *testing.T) {
		// HTTP test server.
		body := "Hello, client\n"
		ts := httptest.NewServer(HTTPHandler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte(body))
		})))
		defer ts.Close()

		// Perform a GET request for previous test server.
		res, err := http.Get(ts.URL)
		if err != nil {
			log.Fatal(err)
		}

		// Check the header
		expected := "Sqreen"
		if got := res.Header.Get("X-Instrumented-By"); got != expected {
			test.Fail(t, expected, got)
		}

		// Check the body
		gotBody, err := ioutil.ReadAll(res.Body)
		if err != nil {
			log.Fatal(err)
		}
		res.Body.Close()
		if got := string(gotBody); got != body {
			test.Fail(t, body, got)
		}
	})
}
