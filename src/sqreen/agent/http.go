package agent

import (
	"net/http"
)

// Return a HTTP handler function inserting header `X-Instrumented-By: Sqreen`
// to the response, and call the given HTTP handler `h`.
func HTTPHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Prefix instrumentation goes here.
		w.Header().Add("X-Instrumented-By", "Sqreen")
		// Call the given handler.
		h.ServeHTTP(w, r)
		// Suffix instrumentation would go here.
	})
}

// Return a HTTP handler function inserting header `X-Instrumented-By: Sqreen`
// to the response, and call the given HTTP handler `h`. This is an helper
// function, with a similar interface to `http.HandlerFunc`, so you can replace
// such calls to the `http` package with this instrumented implementation.
func HTTPHandlerFunc(h func(w http.ResponseWriter, req *http.Request)) http.Handler {
	return HTTPHandler(http.HandlerFunc(h))
}
